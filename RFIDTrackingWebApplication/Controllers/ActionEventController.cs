﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using RFIDTrackingWebApplication.Models;

namespace RFIDTrackingWebApplication.Controllers
{
    public class ActionEventController : Controller
    {

        public readonly string url = "https://localhost:44378/api/";

        // GET: ActionEvent             
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            EventIndex eventModel = new EventIndex();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(url + "ActionEvents"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    eventModel.PersonEventsList = JsonConvert.DeserializeObject<List<ActionEvent>>(apiResponse);
                }
            }
            eventModel.PersonEventsWarningList = eventModel.PersonEventsList.Where(i => i.Temperature > 38);

            
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(url + "GetLocations"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    eventModel.Locations = JsonConvert.DeserializeObject<List<String>>(apiResponse);
                }
            }
            if (eventModel.Locations == null)
            {
                eventModel.Locations = new List<string>();
                string[] lokasyonlar = { "Bahçe", "Kadınlar Tuvaleti", "Erkekler Tuvaleti", "Mutfak", "Öğretmenler Odası", "1A", "2C", "3A", "3B" };
                eventModel.Locations.Add("Bahçe");
                eventModel.Locations.Add("Kadınlar Tuvaleti");
                eventModel.Locations.Add("Erkekler Tuvaleti");
                eventModel.Locations.Add("Mutfak");
                eventModel.Locations.Add("Öğretmenler Odası");
                eventModel.Locations.Add("1A");
                eventModel.Locations.Add("2C");
                eventModel.Locations.Add("3A");
                eventModel.Locations.Add("3B");
            }
            //return PartialView(LocationEvents);
            return View(eventModel);
        }


        // GET: People
        [HttpGet]
        public async Task<IActionResult> GetPeopleAtThisLocations(string location)
        {
            List<Person> peopleList;
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(url + "GetLocationEvents/" + location))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    peopleList = JsonConvert.DeserializeObject<List<Person>>(apiResponse);
                }
            }
            if (peopleList == null)
            {
                peopleList = new List<Person>();
            }
            peopleList.Add(new Person { RecordNo = 123456789, RfidNo = 4656262 ,Size ="S"});
            peopleList.Add(new Person { RecordNo = 234567895, RfidNo = 2515415, Size = "XS" });
            peopleList.Add(new Person { RecordNo = 382541254, RfidNo = 2596965, Size = "XL" });
            peopleList.Add(new Person { RecordNo = 486631458, RfidNo = 1696414, Size = "L" });
            peopleList.Add(new Person { RecordNo = 195212556, RfidNo = 4965965, Size = "M" });
            peopleList.Add(new Person { RecordNo = 567656856, RfidNo = 2596598, Size = "M" });
            peopleList.Add(new Person { RecordNo = 596526265, RfidNo = 6259659, Size = "M" });
            peopleList.Add(new Person { RecordNo = 959596662, RfidNo = 6594488, Size = "M" });
            peopleList.Add(new Person { RecordNo = 826262626, RfidNo = 1484896, Size ="XXL" });
            peopleList.Add(new Person { RecordNo = 264945965, RfidNo = 6259898, Size = "M" });
            peopleList.Add(new Person { RecordNo = 625699659, RfidNo = 9662598, Size = "XXS" });
            peopleList.Add(new Person { RecordNo = 625989595, RfidNo = 5148486, Size = "M" });

            ViewBag.lokasyon = location;

            return View(peopleList);
        }




        [HttpGet]
        public JsonResult GetRow()
        {
            Random rastgele = new Random();
            int rfid = rastgele.Next(10000000, 900000000);
            bool direct = rastgele.Next(0, 2) > 0;
            double temperature = rastgele.Next(340, 390);
            string[] lokasyonlar = { "Bahçe", "Kadınlar Tuvaleti", "Erkekler Tuvaleti", "Mutfak", "Öğretmenler Odası", "1A", "2C", "3A", "3B" };
            int lcno = rastgele.Next(0, lokasyonlar.Length);

            ActionEvent actionEventList = new ActionEvent
            {
                Datetime = DateTime.Now,
                Person = new Person
                {
                    RfidNo = rfid
                },
                Temperature = temperature / 10,
                Antenna = new Antenna
                {
                    Location = lokasyonlar[lcno],
                    Direction = direct
                }
            };

            return Json(actionEventList);
        }
        public static int PreviousId=0;
        // GET: ActionEvent             
        [HttpGet]
        public async Task<IActionResult> getData()
        {
            string apiResponse;
            List<ActionEvent> PersonEventsList = new List<ActionEvent>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(url + "ActionEvents"))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                    PersonEventsList = JsonConvert.DeserializeObject<List<ActionEvent>>(apiResponse);
                }
                ActionEvent lastActionEvent = PersonEventsList.LastOrDefault();
                if (lastActionEvent == null || lastActionEvent.ActionId == PreviousId)
                {
                    return Json(null);
                }
                else
                {
                    PreviousId = lastActionEvent.ActionId;
                    return Json(lastActionEvent);
                }
                
            }
        }
    }
}