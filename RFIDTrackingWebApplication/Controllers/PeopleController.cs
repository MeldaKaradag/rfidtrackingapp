﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using RFIDTrackingWebApplication.Models;

namespace RFIDTrackingWebApplication.Controllers
{
    public class PeopleController : Controller
    {
        String url = "https://localhost:44378/api/People";
        // GET: People
        public async Task<IActionResult> Index()
        {
            List<Person> peopleList = new List<Person>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(url))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    peopleList = JsonConvert.DeserializeObject<List<Person>>(apiResponse);
                }
            }
            return View(peopleList);
        }

        // GET: People/Person/5
        [HttpGet]
        public async Task<IActionResult> Person(int id)
        {
            Person person = new Person();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(url + "/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    person = JsonConvert.DeserializeObject<Person>(apiResponse);
                }
            }
            return View(person);
        }



        /*// GET: People/Create
        public ActionResult Create()
        {
            return View();
        }


        // POST: People/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }*/

        // GET: People/Create
        public ViewResult Create() => View();

        // POST: People/Create
        [HttpPost]
        public async Task<IActionResult> Create(Person person)
        {
            try
            {
                Person receivedperson = new Person();
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(person), Encoding.UTF8, "application/json");

                    using (var response = await httpClient.PostAsync(url, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        receivedperson = JsonConvert.DeserializeObject<Person>(apiResponse);
                    }
                }
                return View(receivedperson);
            }
            catch
            {
                return View();
            }
        }

        /*// GET: People/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: People/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }*/

        // GET: People/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            Person person = new Person();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync((url + "/" + id)))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    person = JsonConvert.DeserializeObject<Person>(apiResponse);
                }
            }
            return View(person);
        }

        // POST: People/Edit/5
        [HttpPost]
        public async Task<IActionResult> Edit(Person person)
        {
            try
            {
                Person receivedPerson = new Person();
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(person), Encoding.UTF8, "application/json");
 
                    using (var response = await httpClient.PutAsync(url + "/" + person.RecordNo, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        ViewBag.Result = "Success";
                        receivedPerson = JsonConvert.DeserializeObject<Person>(apiResponse);
                    }
                }
                return View(receivedPerson);
            }
            catch
            {
                return View();
            }

        }

        // GET: People/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: People/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }


        // POST: People/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteDoctor(int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url);
                await client.DeleteAsync("/" + id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }

        }
    }
}