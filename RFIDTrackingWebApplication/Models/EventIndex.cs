﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFIDTrackingWebApplication.Models
{
    public class EventIndex
    {
        public IEnumerable<ActionEvent>? PersonEventsList { get; set; }
        public IEnumerable<ActionEvent>? PersonEventsWarningList { get; set; }
        public List<string>? Locations { get; set; }
    }

}
