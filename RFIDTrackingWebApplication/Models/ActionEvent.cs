﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFIDTrackingWebApplication.Models
{

    public class ActionEvent
    {
        public int ActionId { get; set; }

        public double Temperature { get; set; }

        public DateTime Datetime { get; set; }

        public Int64 RfidNo { get; set; }

        public long AntennaIdfromReader { get; set; }

        public String ReaderId { get; set; }

        public String Location { get; set; }

        public bool Direction { get; set; }

        public virtual Person Person { get; set; }

        public virtual Antenna Antenna { get; set; }


    }
}
