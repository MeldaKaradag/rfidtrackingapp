﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFIDTrackingWebApplication.Models
{
    public class Antenna
    {
        public int AntenId { get; set; }

        public Int64 AntennaIdfromReader { get; set; }

        public string ReaderId { get; set; }

        public string Location { get; set; }

        public Boolean Direction { get; set; } //true içeri girdi(kapınnın dış tarafında) false dışarı çıktı(kapının iç tarafında)

        public ICollection<ActionEvent> Action { get; set; }
    }
}
