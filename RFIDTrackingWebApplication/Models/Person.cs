﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFIDTrackingWebApplication.Models
{
    public class Person
    {
        public int PersonId { get; set; }

        public int RecordNo { get; set; }

        public Int64? RfidNo { get; set; }

        public string Size { get; set; }

        public List<Person> Friends { get; set; }

        public ICollection<ActionEvent> Action { get; set; }
    }
}
