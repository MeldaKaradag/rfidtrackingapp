﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RFIDTrackingWebAPI.DataAccess;
using RFIDTrackingWebAPI.Model;

namespace RFIDTrackingWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionEventsController : ControllerBase
    {
        private readonly PostgreSqlContext _context;

        public ActionEventsController(PostgreSqlContext context)
        {
            _context = context;
        }

        // GET: api/ActionEvents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActionEvent>>> GetAction()
        {
            return await _context.Action.ToListAsync();
        }

        // GET: api/ActionEvents/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActionEvent>> GetActionEvent(int id)
        {
            var actionEvent = await _context.Action.FindAsync(id);

            if (actionEvent == null)
            {
                return NotFound();
            }

            return actionEvent;
        }

        // PUT: api/ActionEvents/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActionEvent(int id, ActionEvent actionEvent)
        {
            if (id != actionEvent.ActionId)
            {
                return BadRequest();
            }

            _context.Entry(actionEvent).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActionEventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ActionEvents
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ActionEvent>> PostActionEvent(ActionEvent actionEvent)
        {
            _context.Action.Add(actionEvent);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActionEvent", new { id = actionEvent.ActionId }, actionEvent);
        }

        // DELETE: api/ActionEvents/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActionEvent>> DeleteActionEvent(int id)
        {
            var actionEvent = await _context.Action.FindAsync(id);
            if (actionEvent == null)
            {
                return NotFound();
            }

            _context.Action.Remove(actionEvent);
            await _context.SaveChangesAsync();

            return actionEvent;
        }

        private bool ActionEventExists(int id)
        {
            return _context.Action.Any(e => e.ActionId == id);
        }
    }
}
