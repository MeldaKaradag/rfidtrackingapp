﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RFIDTrackingWebAPI.Model;
using Microsoft.EntityFrameworkCore;
using RFIDTrackingWebAPI.DataAccess;


namespace RFIDTrackingWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReaderController : ControllerBase
    {
        private readonly PostgreSqlContext _context;

        public ReaderController(PostgreSqlContext context)
        {
            _context = context;
        }


        [HttpPost]
        public void Data(ReaderModel reader)
        {
            ActionEvent actionEvent = new ActionEvent
            {
                Temperature = Convert.ToDouble(reader.strmyData1),
                Datetime = Convert.ToDateTime(reader.strmyTime),
                RfidNo = Int64.Parse(reader.strmyEpc),
                AntennaIdfromReader = long.Parse(reader.strmyAntenId),
                ReaderId = reader.strmyReaderId,
                Location = reader.strmyAntenDir,
                Direction = bool.Parse(reader.strmyPC)
            };

            _context.Action.Add(actionEvent);
            _context.SaveChanges();

        }

    }
}