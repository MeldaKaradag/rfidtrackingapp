﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using RFIDTrackingWebAPI.DataAccess;
using RFIDTrackingWebAPI.Model;

namespace RFIDTrackingWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly PostgreSqlContext _context;

        public LocationsController(PostgreSqlContext context)
        {
            _context = context;
        }

        // GET: api/Locations
        [HttpGet]
        public async Task<IList> GetLocations()
        {
            var locations = _context.Antenna.Select(m => m.Location).Distinct().ToListAsync();
            return await locations;
        }

        // GET: api/GetLocationEvents/location
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Person>>> GetLocationEvents(string location)
        {
            var PersonListIncurrentLocation = (from actionevent in _context.Action
                                               where actionevent.Antenna.Location.Equals(location) || actionevent.Antenna.Direction == false
                                               select actionevent.Person);

            return await PersonListIncurrentLocation.ToListAsync();
        }




    }
}