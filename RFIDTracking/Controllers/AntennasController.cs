﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RFIDTrackingWebAPI.DataAccess;
using RFIDTrackingWebAPI.Model;

namespace RFIDTrackingWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AntennasController : ControllerBase
    {
        private readonly PostgreSqlContext _context;

        public AntennasController(PostgreSqlContext context)
        {
            _context = context;
        }

        // GET: api/Antennas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Antenna>>> GetAntenna()
        {
            return await _context.Antenna.ToListAsync();
        }

        // GET: api/Antennas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Antenna>> GetAntenna(long id)
        {
            var antenna = await _context.Antenna.FindAsync(id);

            if (antenna == null)
            {
                return NotFound();
            }

            return antenna;
        }

        // PUT: api/Antennas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAntenna(long id, Antenna antenna)
        {
            if (id != antenna.AntenId)
            {
                return BadRequest();
            }

            _context.Entry(antenna).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AntennaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Antennas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Antenna>> PostAntenna(Antenna antenna)
        {
            _context.Antenna.Add(antenna);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAntenna", new { id = antenna.AntenId }, antenna);
        }

        // DELETE: api/Antennas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Antenna>> DeleteAntenna(long id)
        {
            var antenna = await _context.Antenna.FindAsync(id);
            if (antenna == null)
            {
                return NotFound();
            }

            _context.Antenna.Remove(antenna);
            await _context.SaveChangesAsync();

            return antenna;
        }

        private bool AntennaExists(long id)
        {
            return _context.Antenna.Any(e => e.AntenId == id);
        }
    }
}
