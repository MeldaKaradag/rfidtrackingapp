﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RFIDTrackingWebAPI.Model;

namespace RFIDTrackingWebAPI.DataAccess
{
    public class PostgreSqlContext : DbContext
    {
        public PostgreSqlContext(DbContextOptions<PostgreSqlContext> options) : base(options)
        {
            
        }

        public DbSet<ActionEvent> Action { get; set; }
        public DbSet<Antenna> Antenna { get; set; }
        public DbSet<Person> Person { get; set; }


        /*protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .HasOne(a => a.rfid)
                .WithOne(b => b.person)
                .HasForeignKey<Person>(b => b.tc);


        }*/

    }
}
