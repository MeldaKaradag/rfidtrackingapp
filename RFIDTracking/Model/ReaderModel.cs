﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFIDTrackingWebAPI.Model
{
    public class ReaderModel
    {
        public String strmyAntenId { get; set; }
        public String strmyEpc { get; set; }
        public String strmyTime { get; set; }
        public String strmyData1 { get; set; }
        public String strmyData2 { get; set; }
        public String strmyPC { get; set; }
        public String strmyReaderId { get; set; }
        public String strmyAntenDir { get; set; }
        public String strmyHareket { get; set; }
    }
}