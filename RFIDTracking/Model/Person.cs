﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace RFIDTrackingWebAPI.Model
{
    [Table("Person")]
    public class Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PersonId { get; set; }

        public int RecordNo { get; set; }

        public Int64? RfidNo { get; set; }

        public string Size { get; set; }

        public List<Person> Friends { get; set; }

        public ICollection<ActionEvent> Action { get; set; }
    }
}
