﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RFIDTrackingWebAPI.Model
{
    [Table("Antenna")]
    public class Antenna
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AntenId { get; set; }

        public Int64 AntennaIdfromReader { get; set; }

        public string ReaderId { get; set; }

        public string Location { get; set; }

        public Boolean Direction { get; set; } //true içeri girdi(kapınnın dış tarafında) false dışarı çıktı(kapının iç tarafında)

        public ICollection<ActionEvent> Action { get; set; }
    }
}