﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace RFIDTrackingWebAPI.Model
{
    [Table("ActionEvent")]
    public class ActionEvent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ActionId { get; set; }

        public double Temperature { get; set; }

        public DateTime Datetime { get; set; }

        public Int64 RfidNo { get; set; }

        public long AntennaIdfromReader { get; set; }

        public String ReaderId { get; set; }

        public String Location { get; set; }

        public bool Direction { get; set; }

        [JsonIgnore]
        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }
 
        [JsonIgnore]
        [ForeignKey("AntennaId")]
        public virtual Antenna Antenna { get; set; }
    }
}
